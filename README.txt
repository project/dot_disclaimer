 Dot Disclaimer - Readme

This module adds custom disclaimers.

Features:

1. Allows adding seperate disclaimers for registration, content creation and comments creation.
2. Uses nodes for disclaimer content, to take advantage of revisions.
3. Allows forcing users to select a checkbox with customizable text.
3. Simple Installation

Installation:

1. Upload to modules directory
2. Goto admin/modules and activate
3. Goto admin/settings/dot_disclaimer to setup
4. optional - goto admin/access and set bypass for admin user role 
